import React from 'react';
import { View, Image, Text } from 'react-native';
import PropTypes from 'prop-types';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';

import avatar from '../assets/images/avatar.png';
import styles from '../styles/components/Actor';

function Actor({ name, image }) {
  return name && image ? (
    <View style={styles.container}>
      <Image
        source={image !== 'https://image.tmdb.org/t/p/w500null' ? { uri: image } : avatar}
        style={styles.image}
        resizeMode="cover"
      />
      <Text style={styles.text} numberOfLines={1}>
        {name}
      </Text>
    </View>
  ) : (
    <View style={styles.container}>
      <ShimmerPlaceHolder LinearGradient={LinearGradient} shimmerStyle={styles.image} />
      <ShimmerPlaceHolder LinearGradient={LinearGradient} shimmerStyle={styles.text} />
    </View>
  );
}

Actor.propTypes = {
  name: PropTypes.string,
  image: PropTypes.string,
};

Actor.defaultProps = {
  name: null,
  image: null,
};

export default Actor;
