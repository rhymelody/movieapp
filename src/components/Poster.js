import React from 'react';
import { TouchableOpacity, Image, View } from 'react-native';
import PropTypes from 'prop-types';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';

import styles from '../styles/components/Poster';

function Poster({ data, navigation }) {
  return data ? (
    <TouchableOpacity
      style={styles.container}
      onPress={() => navigation.navigate('Movie Detail', { id: data.id })}
    >
      <Image
        source={{
          uri: data.poster_path,
        }}
        style={styles.image}
        resizeMode="cover"
      />
    </TouchableOpacity>
  ) : (
    <View style={styles.container}>
      <ShimmerPlaceHolder LinearGradient={LinearGradient} shimmerStyle={styles.image} />
    </View>
  );
}

Poster.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.number,
    poster_path: PropTypes.string,
  }),
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }),
};

Poster.defaultProps = {
  data: null,
  navigation: null,
};

export default Poster;
