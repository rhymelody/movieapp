import Poster from './Poster';
import Movie from './Movie';
import Actor from './Actor';

export { Poster, Movie, Actor };
