import React from 'react';
import { View, TouchableOpacity, Image, Text, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';
import Moment from 'moment';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';

import styles from '../styles/components/Movie';

function Movie({ data, navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.contentImage}>
        {data ? (
          <Image
            style={styles.contentImage.image}
            resizeMode="cover"
            source={{
              uri: data.poster_path,
            }}
          />
        ) : (
          <ShimmerPlaceHolder
            LinearGradient={LinearGradient}
            shimmerStyle={styles.contentImage.image}
          />
        )}
      </View>
      <View style={styles.contentDetails}>
        <Text style={styles.contentDetails.title} numberOfLines={1}>
          {data ? (
            data.title
          ) : (
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              shimmerStyle={styles.shimmer.title}
              shimmerColors={styles.shimmer.title.color}
            />
          )}
        </Text>
        <View style={styles.contentDetails.row}>
          {data ? (
            <Text style={styles.contentDetails.release}>
              {Moment(data.release_date).format('MMM d YYYY ')}
            </Text>
          ) : (
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              shimmerStyle={styles.shimmer.release}
            />
          )}
          {data ? (
            <View style={styles.contentDetails.rating}>
              <Icon name="star-sharp" style={styles.contentDetails.rating.icon} />
              <Text style={styles.contentDetails.rating.text}>{data.vote_average}</Text>
            </View>
          ) : (
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              shimmerStyle={styles.shimmer.rating}
              shimmerColors={styles.shimmer.rating.color}
            />
          )}
        </View>
        <Text style={styles.contentDetails.genre}>
          {data ? (
            'Action, Comedy, Adult'
          ) : (
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              shimmerStyle={[
                styles.shimmer.genre,
                Platform.OS === 'ios' && styles.shimmer.genre.ios,
              ]}
            />
          )}
        </Text>
        {data ? (
          <TouchableOpacity
            style={styles.contentDetails.button}
            onPress={() => navigation.navigate('Movie Detail', { id: data.id })}
          >
            <Icon name="tv-outline" style={styles.contentDetails.button.icon} />
            <Text style={styles.contentDetails.button.text}>Watch</Text>
          </TouchableOpacity>
        ) : (
          <ShimmerPlaceHolder
            LinearGradient={LinearGradient}
            shimmerStyle={styles.shimmer.button}
            shimmerColors={styles.shimmer.button.color}
          />
        )}
      </View>
    </View>
  );
}

Movie.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    release_date: PropTypes.string.isRequired,
    vote_average: PropTypes.number.isRequired,
    poster_path: PropTypes.string.isRequired,
  }),
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }),
};

Movie.defaultProps = {
  data: null,
  navigation: null,
};

export default Movie;
