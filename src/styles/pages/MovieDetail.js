import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  $headerMinHeight: '5rem',
  $headerMaxHeight: '12rem',
  safeArea: {
    backgroundColor: '$background',
    height: '100%',
    flexGrow: 1,
  },
  header: {
    minHeight: '$headerMinHeight',
    maxHeight: '$headerMaxHeight + 4.75rem',
    backgroundColor: '$background',
    image: {
      height: '$headerMaxHeight',
      width: '100%',
      borderBottomLeftRadius: '.425rem',
      borderBottomRightRadius: '.425rem',
    },
    navigation: {
      position: 'absolute',
      width: '100%',
      height: '5rem',
      paddingHorizontal: '1.5rem',
      flexDirection: 'row',
      action: {
        button: {
          backgroundColor: 'rgba(0, 0, 0, 0.675)',
          height: '2.25rem',
          width: '2.25rem',
          borderRadius: '2.25rem / 2',
          alignItems: 'center',
          justifyContent: 'center',
        },
        icon: {
          color: '$base.white',
          fontSize: '1.275rem',
        },
      },
      actionLeft: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
      },
      actionRight: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
      },
    },
    movie: {
      flex: 1,
      flexGrow: 1,
      justifyContent: 'flex-end',
      alignItems: 'center',
      paddingHorizontal: '1.5rem',
      content: {
        backgroundColor: '#161616',
        borderRadius: '1rem',
        width: '100%',
        height: '11.5rem',
        paddingVertical: '1.125rem',
        paddingHorizontal: '1rem',
        flexDirection: 'row',
        contentImage: {
          justifyContent: 'center',
          alignItems: 'center',
          marginRight: '.5rem',
          image: {
            height: '9.25rem',
            width: '6rem',
            borderRadius: '.5rem',
          },
        },
        contentDetails: {
          flexShrink: 1,
          paddingVertical: '1rem',
          paddingLeft: '.5rem',
          paddingRight: '1rem',
          justifyContent: 'center',
          title: {
            flexGrow: 1,

            color: '$base.white',
            fontSize: '1.25rem',
            fontWeight: 'bold',
            marginBottom: '.75rem',
          },
          tagline: {
            flexGrow: 1,

            color: '$secondary[100]',
            fontSize: '.825rem',
            marginBottom: '.25rem',
          },
          release: {
            flexGrow: 1,

            color: '$secondary[100]',
            fontSize: '.825rem',
            marginBottom: '.75rem',
          },
          row: {
            flexDirection: 'row',
            flexGrow: 1,
            alignItems: 'center',
            justifyContent: 'space-between',
            status: {
              color: '$secondary[100]',
              fontSize: '.825rem',
              marginRight: '1rem',
            },
            runtime: {
              color: '$secondary[100]',
              fontSize: '.825rem',
              marginRight: '1rem',
            },
            rating: {
              flexDirection: 'row',
              alignItems: 'center',
              marginRight: '1rem',
              icon: {
                color: '#f1c40e',
                fontSize: '.775rem',
              },
              text: {
                fontSize: '.775rem',
                color: '#f1c40e',
                marginLeft: '.275rem',
              },
            },
          },
        },
      },
    },
  },
  container: {
    backgroundColor: '$background',
    paddingVertical: '1.25rem',
    paddingHorizontal: '2.25rem',
    width: '100%',
    minHeight: '100%',
    flexGrow: 1,
  },
  content: {
    genre: {
      marginBottom: '2rem',
      header: {
        color: '$base.white',
        fontSize: '1.125rem',
        fontWeight: 'bold',
        marginBottom: '.75rem',
      },
      content: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        badge: {
          backgroundColor: '$secondary[300]',
          paddingVertical: '.275rem',
          paddingHorizontal: '1rem',
          borderRadius: '.5rem',
          marginRight: '.75rem',
          marginBottom: '.75rem',
          height: '1.875rem',
          justifyContent: 'center',
          alignItems: 'center',
          text: {
            color: '$base.black',
            fontSize: '.875rem',
          },
        },
      },
    },
    synopshis: {
      marginTop: '-.75rem',
      marginBottom: '2rem',
      header: {
        color: '$base.white',
        fontSize: '1.125rem',
        fontWeight: 'bold',
        marginBottom: '.75rem',
      },
      content: {
        color: '$secondary[300]',
        fontSize: '.875rem',
      },
    },
    actor: {
      marginBottom: '2rem',
      header: {
        color: '$base.white',
        fontSize: '1.125rem',
        fontWeight: 'bold',
        marginBottom: '.75rem',
      },
      content: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginHorizontal: '-.375rem',
      },
    },
  },
  shimmer: {
    title: {
      width: '10rem',
      marginBottom: '.75rem',
      color: ['#8f959d', '#666666', '#8f959d'],
      borderRadius: '.25rem',
    },
    tagline: {
      width: '6.75rem',
      marginBottom: '.25rem',
      borderRadius: '.25rem',
    },
    release: {
      width: '5rem',
      marginRight: '1rem',
      marginBottom: '.75rem',
      borderRadius: '.25rem',
    },
    status: {
      width: '3rem',
      marginRight: '1rem',
      borderRadius: '.25rem',
    },
    runtime: {
      width: '4.25rem',
      marginRight: '1rem',
      borderRadius: '.25rem',
    },
    rating: {
      width: '2.5rem',
      marginRight: '1rem',
      color: ['#dea36c', '#ebebeb', '#dea36c'],
      borderRadius: '.25rem',
    },
    genre: {
      width: '5.75rem',
      height: '1.875rem',
      borderRadius: '.5rem',
      marginRight: '.75rem',
      marginBottom: '.75rem',
      color: ['#8f959d', '#d5dbe1', '#8f959d'],
    },
    synopsis: {
      marginBottom: '.3rem',
      borderRadius: '.25rem',
    },
  },
});

export default styles;
