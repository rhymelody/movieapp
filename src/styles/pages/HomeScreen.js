import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  safeArea: {
    backgroundColor: '$background',
    height: '100%',
  },
  container: {
    padding: '1.5rem',
    width: '100%',
    flexGrow: 1,
  },
  recomendation: {
    marginBottom: '2rem',
    header: {
      color: '$base.white',
      fontSize: '1.125rem',
      fontWeight: 'bold',
      marginBottom: '1rem',
    },
    content: {
      flexDirection: 'row',
      marginHorizontal: '-.5rem',
    },
  },
  latestUpload: {
    header: {
      color: '$base.white',
      fontSize: '1.125rem',
      fontWeight: 'bold',
      marginBottom: '1rem',
    },
    content: {},
  },
});

export default styles;
