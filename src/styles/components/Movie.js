import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  container: {
    flexDirection: 'row',
    marginBottom: '1.25rem',
    flexGrow: 1,
  },
  contentImage: {
    justifyContent: 'center',
    marginRight: '.5rem',
    image: {
      width: '7.75rem',
      height: '7.75rem',
      borderRadius: '.25rem',
    },
  },
  contentDetails: {
    marginLeft: '.5rem',
    flexShrink: 1,
    justifyContent: 'center',
    flexWrap: 'nowrap',
    paddingVertical: '.25rem',
    title: {
      color: '$base.white',
      fontSize: '.975rem',
      fontWeight: 'bold',
      marginBottom: '.375rem',
    },
    row: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    release: {
      color: '$secondary[300]',
      fontSize: '.825rem',
      marginRight: '1rem',
    },
    rating: {
      flexDirection: 'row',
      alignItems: 'center',
      icon: {
        color: '#f1c40e',
        fontSize: '.775rem',
      },
      text: {
        fontSize: '.775rem',
        color: '#f1c40e',
        marginLeft: '.275rem',
      },
    },
    genre: {
      color: '$secondary[300]',
      fontSize: '.825rem',
    },
    button: {
      marginTop: '.875rem',
      backgroundColor: '$primary[500]',
      padding: '.425rem',
      borderRadius: '.25rem',
      width: '6.25rem',
      height: '2.125rem',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      icon: {
        color: '$base.white',
        fontSize: '1.125rem',
        marginRight: '.25rem',
      },
      text: {
        top: -1,
        color: '$base.white',
        fontSize: '.875rem',
        marginLeft: '.25rem',
      },
    },
  },
  shimmer: {
    title: {
      width: '10rem',
      marginBottom: '.375rem',
      color: ['#8f959d', '#666666', '#8f959d'],
      borderRadius: '.25rem',
    },
    release: {
      width: '5rem',
      marginRight: '1rem',
      marginTop: '.25rem',
      marginBottom: '.5rem',
      borderRadius: '.25rem',
    },
    rating: {
      width: '2.5rem',
      marginTop: '.25rem',
      marginBottom: '.5rem',
      color: ['#dea36c', '#ebebeb', '#dea36c'],
      borderRadius: '.25rem',
    },
    genre: {
      width: '8rem',
      marginBottom: '.5rem',
      borderRadius: '.25rem',
      ios: {
        height: '.45rem',
      },
    },
    button: {
      marginTop: '.875rem',
      width: '6.25rem',
      height: '2.125rem',
      borderRadius: '.25rem',
      color: ['#d82047', '#711b24', '#d82047'],
    },
  },
});

export default styles;
