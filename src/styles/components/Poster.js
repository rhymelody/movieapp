import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  container: {
    marginHorizontal: '.5rem',
  },
  image: {
    height: '10.25rem',
    width: '7.125rem',
    borderRadius: '.5rem',
  },
});

export default styles;
