import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  container: {
    alignItems: 'center',
    width: '33.3333333333%',
    flexGrow: 0,
    flexShrink: 0,
    flexBasis: 'auto',
  },
  image: {
    height: '9.25rem',
    width: '6rem',
    borderRadius: '.5rem',
    marginBottom: '.5rem',
  },
  text: {
    flexGrow: 1,
    color: '$secondary[100]',
    fontSize: '.75rem',
    textAlign: 'center',
    marginBottom: '1.25rem',
    width: '85%',
    borderRadius: '.3rem',
  },
});

export default styles;
