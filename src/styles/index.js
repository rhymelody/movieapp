import EStyleSheet from 'react-native-extended-stylesheet';

EStyleSheet.build({
  $background: '#000000',
  $primary: {
    900: '#6e0b47',
    700: '#a51d55',
    500: '#d82047',
    300: '#f7888b',
    100: '#fdddd7',
  },
  $secondary: {
    900: '#1b274b',
    700: '#485671',
    500: '#8f959d',
    300: '#d5dbe1',
    100: '#f5f8fa',
  },
  $netral: {
    900: '#050b16',
    700: '#0f1721',
    500: '#1e272e',
    300: '#98b3c0',
    100: '#e3f0f4',
  },
  $info: {
    900: '#003864',
    700: '#006c96',
    500: '#00b8d1',
    300: '#60f1eb',
    100: '#cafcf2',
  },
  $warning: {
    900: '#716100',
    700: '#aa9501',
    500: '#edd502',
    300: '#f9ed65',
    100: '#fefbcb',
  },
  $danger: {
    900: '#6c0a37',
    700: '#a21b41',
    500: '#e23647',
    300: '#f68f85',
    100: '#fde1d6',
  },
  $success: {
    900: '#09571a',
    700: '#17831e',
    500: '#3db72f',
    300: '#9ce981',
    100: '#e5fbd6',
  },
  $base: {
    primary: '$primary[500]',
    secondary: '$secondary[500]',
    info: '$info[500]',
    success: '$success[500]',
    warning: '$warning[500]',
    danger: '$danger[500]',
    border: '#dadce0',
    white: '#ffffff',
    black: '#000000',
  },
});

export default EStyleSheet;
