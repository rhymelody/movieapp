import getMovie from './get-movie';
import getMovieList from './get-movie-list';

export { getMovie, getMovieList };
