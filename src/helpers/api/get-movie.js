import axios from 'axios';
import { BASE_API_URL } from '..';

const getMovie = async (id) => {
  const response = await axios.get(`${BASE_API_URL}/movies/${id}`);
  return response;
};

export default getMovie;
