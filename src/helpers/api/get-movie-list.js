import axios from 'axios';
import { BASE_API_URL } from '..';

const getMovieList = async () => {
  const response = await axios.get(`${BASE_API_URL}/movies`);

  return response;
};

export default getMovieList;
