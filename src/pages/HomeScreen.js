import React, { useState } from 'react';
import { View, Text, ScrollView, RefreshControl } from 'react-native';
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import EStyleSheet from 'react-native-extended-stylesheet';
import PropTypes from 'prop-types';
import { showMessage } from 'react-native-flash-message';
import { useQuery } from 'react-query';

import { getMovieList } from '../helpers/api';
import { Poster, Movie } from '../components';
import styles from '../styles/pages/HomeScreen';

function HomeScreen({ navigation }) {
  const [recommendedMovie, setRecommendedMovie] = useState(null);
  const [latestMovie, setLatestMovie] = useState(null);
  const insets = useSafeAreaInsets();

  const { isFetching, isError, refetch } = useQuery(
    'movies',
    async () => {
      try {
        const response = await getMovieList();
        return response.data;
      } catch (error) {
        throw new Error(error.message);
      }
    },
    {
      onSuccess: (data) => {
        const recommended = data.results
          .sort((min, max) => {
            return max.vote_average - min.vote_average;
          })
          .slice(0, 5);
        setRecommendedMovie(recommended);
        const latest = data.results
          .sort((min, max) => {
            return new Date(min.release_date) - new Date(max.release_date);
          })
          .slice(0, 10);
        setLatestMovie(latest);
      },
      onError: (error) => {
        showMessage({
          message: `[${error.name.toUpperCase()}] ${error.message}`,
          type: 'danger',
          backgroundColor: EStyleSheet.value('$danger[500]'),
          titleStyle: { textAlign: 'center' },
          floating: true,
          statusBarHeight: insets.top + 6,
        });
      },
    }
  );

  return (
    <SafeAreaView style={styles.safeArea}>
      <ScrollView
        contentContainerStyle={styles.container}
        refreshControl={<RefreshControl refreshing={isFetching} onRefresh={refetch} />}
      >
        <View style={styles.recomendation}>
          <Text style={styles.recomendation.header}>Recommended</Text>
          <ScrollView
            horizontal
            contentContainerStyle={styles.recomendation.content}
            showsHorizontalScrollIndicator={false}
          >
            {isFetching || isError
              ? [1, 2, 3, 4, 5].map((data) => <Poster key={data} />)
              : recommendedMovie.map((data) => (
                  <Poster key={data.id} data={data} navigation={navigation} />
                ))}
          </ScrollView>
        </View>
        <View style={styles.latestUpload}>
          <Text style={styles.latestUpload.header}>Latest Upload</Text>
          <View style={styles.latestUpload.content}>
            {isFetching || isError
              ? [1, 2, 3, 4, 5].map((data) => <Movie key={data} />)
              : latestMovie.map((data) => (
                  <Movie key={data.id} data={data} navigation={navigation} />
                ))}
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

HomeScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default HomeScreen;
