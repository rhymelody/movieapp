import HomeScreen from './HomeScreen';
import MovieDetail from './MovieDetail';

export { HomeScreen, MovieDetail };
