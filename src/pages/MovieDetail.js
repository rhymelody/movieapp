import React, { useState } from 'react';
import { View, Text, Image, TouchableOpacity, Share, RefreshControl } from 'react-native';
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import EStyleSheet from 'react-native-extended-stylesheet';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import Icon from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';
import { showMessage } from 'react-native-flash-message';
import { useQuery } from 'react-query';
import Moment from 'moment';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';

import { getMovie } from '../helpers/api';
import styles from '../styles/pages/MovieDetail';
import { Actor } from '../components';

function MovieDetail({ route, navigation }) {
  const [movieData, setMovieData] = useState(null);
  const insets = useSafeAreaInsets();

  const { isFetching, isError, refetch } = useQuery(
    'movie-detail',
    async () => {
      try {
        const response = await getMovie(route.params.id);
        return response.data;
      } catch (error) {
        throw new Error(error.message);
      }
    },
    {
      onSuccess: (response) => {
        setMovieData(response);
      },
      onError: (error) => {
        showMessage({
          message: `[${error.name.toUpperCase()}] ${error.message}`,
          type: 'danger',
          backgroundColor: EStyleSheet.value('$danger[500]'),
          titleStyle: { textAlign: 'center' },
          floating: true,
          statusBarHeight: insets.top + 6,
        });
      },
    }
  );

  return (
    <SafeAreaView style={styles.safeArea}>
      <ParallaxScrollView
        style={{ overflow: 'hidden' }}
        refreshControl={<RefreshControl refreshing={isFetching} onRefresh={refetch} />}
        stickyHeaderHeight={styles.header.minHeight}
        parallaxHeaderHeight={styles.header.maxHeight}
        contentBackgroundColor={styles.header.backgroundColor}
        headerBackgroundColor={styles.header.backgroundColor}
        renderBackground={() =>
          isFetching || isError ? (
            <ShimmerPlaceHolder LinearGradient={LinearGradient} style={styles.header.image} />
          ) : (
            <Image
              key="background"
              source={{
                uri: movieData.backdrop_path,
              }}
              style={[styles.header.image, { opacity: 0.5 }]}
            />
          )
        }
        renderForeground={() => (
          <View key="parallax-header" style={styles.header.movie}>
            <View style={styles.header.movie.content}>
              <View style={styles.header.movie.content.contentImage}>
                {isFetching || isError ? (
                  <ShimmerPlaceHolder
                    LinearGradient={LinearGradient}
                    style={styles.header.movie.content.contentImage.image}
                  />
                ) : (
                  <Image
                    style={styles.header.movie.content.contentImage.image}
                    resizeMode="cover"
                    source={{
                      uri: movieData.backdrop_path,
                    }}
                  />
                )}
              </View>
              <View style={styles.header.movie.content.contentDetails}>
                <Text
                  style={styles.header.movie.content.contentDetails.title}
                  adjustsFontSizeToFit
                  numberOfLines={2}
                >
                  {isFetching || isError ? (
                    <ShimmerPlaceHolder
                      LinearGradient={LinearGradient}
                      shimmerStyle={styles.shimmer.title}
                      shimmerColors={styles.shimmer.title.color}
                    />
                  ) : (
                    movieData.title
                  )}
                </Text>
                <Text style={styles.header.movie.content.contentDetails.tagline}>
                  {isFetching || isError ? (
                    <ShimmerPlaceHolder
                      LinearGradient={LinearGradient}
                      shimmerStyle={styles.shimmer.tagline}
                    />
                  ) : (
                    movieData.tagline
                  )}
                </Text>
                <Text style={styles.header.movie.content.contentDetails.release}>
                  {isFetching || isError ? (
                    <ShimmerPlaceHolder
                      LinearGradient={LinearGradient}
                      shimmerStyle={styles.shimmer.release}
                    />
                  ) : (
                    Moment(movieData.release_date).format('MMM d YYYY ')
                  )}
                </Text>
                <View style={styles.header.movie.content.contentDetails.row}>
                  <Text style={styles.header.movie.content.contentDetails.row.status}>
                    {isFetching || isError ? (
                      <ShimmerPlaceHolder
                        LinearGradient={LinearGradient}
                        shimmerStyle={styles.shimmer.status}
                      />
                    ) : (
                      movieData.status
                    )}
                  </Text>
                  <Text style={styles.header.movie.content.contentDetails.row.runtime}>
                    {isFetching || isError ? (
                      <ShimmerPlaceHolder
                        LinearGradient={LinearGradient}
                        shimmerStyle={styles.shimmer.runtime}
                      />
                    ) : (
                      `${Math.floor(movieData.runtime / 60)}h ${movieData.runtime % 60}min`
                    )}
                  </Text>
                  <View style={styles.header.movie.content.contentDetails.row.rating}>
                    {isFetching || isError ? (
                      <ShimmerPlaceHolder
                        LinearGradient={LinearGradient}
                        shimmerStyle={styles.shimmer.rating}
                        shimmerColors={styles.shimmer.rating.color}
                      />
                    ) : (
                      <>
                        <Icon
                          name="star-sharp"
                          style={styles.header.movie.content.contentDetails.row.rating.icon}
                        />
                        <Text style={styles.header.movie.content.contentDetails.row.rating.text}>
                          {movieData.vote_average}
                        </Text>
                      </>
                    )}
                  </View>
                </View>
              </View>
            </View>
          </View>
        )}
        renderStickyHeader={() => <View key="sticky-header" style={styles.header.navigation} />}
        renderFixedHeader={() => (
          <View key="fixed-header" style={styles.header.navigation}>
            <View style={styles.header.navigation.actionLeft}>
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <View style={styles.header.navigation.action.button}>
                  <Icon name="arrow-back-outline" style={styles.header.navigation.action.icon} />
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.header.navigation.actionRight}>
              <TouchableOpacity
                style={{ marginRight: 5 }}
                onPress={() =>
                  showMessage({
                    message: 'You like this movie!',
                    type: 'default',
                    backgroundColor: EStyleSheet.value('$primary[500]'),
                    style: { opacity: 0.925 },
                    titleStyle: { textAlign: 'center' },
                    floating: true,
                    statusBarHeight: insets.top + 6,
                  })
                }
              >
                <View style={styles.header.navigation.action.button}>
                  <Icon name="heart-outline" style={styles.header.navigation.action.icon} />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{ marginLeft: 5 }}
                onPress={() =>
                  Share.share(
                    {
                      title: 'Movieku',
                      message: `Watch ${
                        isFetching || isError ? 'Movie' : movieData.title
                      } in best Experience only on Movieku!`,
                    },
                    { dialogTitle: 'Movieku' }
                  )
                }
              >
                <View style={styles.header.navigation.action.button}>
                  <Icon name="arrow-redo-outline" style={styles.header.navigation.action.icon} />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        )}
      >
        <View style={styles.container}>
          <View style={styles.content.genre}>
            <Text style={styles.content.genre.header}>Genre</Text>
            <View style={styles.content.genre.content}>
              {isFetching || isError
                ? [1, 2, 3].map((value) => (
                    <ShimmerPlaceHolder
                      key={value}
                      LinearGradient={LinearGradient}
                      shimmerStyle={styles.shimmer.genre}
                      shimmerColors={styles.shimmer.genre.color}
                    />
                  ))
                : movieData.genres.map((value) => (
                    <View style={styles.content.genre.content.badge} key={value.id}>
                      <Text style={styles.content.genre.content.badge.text}>{value.name}</Text>
                    </View>
                  ))}
            </View>
          </View>
          <View style={styles.content.synopshis}>
            <Text style={styles.content.synopshis.header}>Synopsis</Text>
            {isFetching || isError ? (
              ['75%', '90%', '87.5%', '95%', '65%'].map((value) => (
                <ShimmerPlaceHolder
                  LinearGradient={LinearGradient}
                  shimmerStyle={[styles.shimmer.synopsis, { width: value }]}
                  key={value}
                />
              ))
            ) : (
              <Text style={styles.content.synopshis.content}>{movieData.overview}</Text>
            )}
          </View>
          <View style={styles.content.actor}>
            <Text style={styles.content.actor.header}>Actors/Artists</Text>
            <View style={styles.content.actor.content}>
              {isFetching || isError
                ? [1, 2, 3, 4, 5, 6].map((value) => <Actor key={value} />)
                : movieData.credits.cast.map((value) => (
                    <Actor name={value.name} image={value.profile_path} key={value.id} />
                  ))}
            </View>
          </View>
        </View>
      </ParallaxScrollView>
    </SafeAreaView>
  );
}

MovieDetail.propTypes = {
  route: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.number.isRequired,
    }).isRequired,
  }).isRequired,
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};

export default MovieDetail;
